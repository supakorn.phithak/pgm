﻿using PGM.Model;
using PGM.Context.EF;
using PGM.Model.Product.Parameter;

namespace PGM.Interface
{
    public interface IProduct
    {
        public List<ResponseModel> _getListShipment(string shipDate);

        public List<ResponseModel> _getListTrip(reqNumberModel req);

        public List<CheckInTimeModel> _getNumberThree(reqNumberModel req);
    }
}
