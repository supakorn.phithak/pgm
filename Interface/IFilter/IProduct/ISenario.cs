﻿using PGM.Model;
using PGM.Model.Product.Parameter;

namespace PGM.Interface.IFilter.IProduct
{
    public interface ISenario
    {
        public List<CheckInTimeModel> Process(reqNumberModel req);
    }
}
