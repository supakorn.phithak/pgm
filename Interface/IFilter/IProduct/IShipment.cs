﻿using PGM.Context.EF;
using PGM.Model;
using PGM.Model.Product.Parameter;

namespace PGM.Interface.IFilter.IProduct
{
    public interface IShipment
    {
        public List<ResponseModel> Process(string shipDate);
    }
}
