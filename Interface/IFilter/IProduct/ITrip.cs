﻿using PGM.Model;
using PGM.Model.Product.Parameter;

namespace PGM.Interface.IFilter.IProduct
{
    public interface ITrip
    {
        public List<ResponseModel> Process(reqNumberModel req);
    }
}
