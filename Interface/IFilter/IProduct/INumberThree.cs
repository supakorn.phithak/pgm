﻿using PGM.Model;
using PGM.Model.Product.Parameter;
using PGM.Model.Product.Senario;

namespace PGM.Interface.IFilter.IProduct
{
    public interface INumberThree
    {
        public void Process(ref SenarioListModel senario, reqNumberModel req);
    }
}
