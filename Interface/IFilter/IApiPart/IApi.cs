﻿using PGM.Model;
using PGM.Context.EF;
using PGM.Model.Product.Parameter;
using PGM.Model.ApiPart;

namespace PGM.Interface.IFilter.IApiPart
{
    public interface IApi
    {
        public ResponseApiModel Process(RequestApiModel req);

    }
}
