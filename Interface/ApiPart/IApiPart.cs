﻿using PGM.Model;
using PGM.Context.EF;
using PGM.Model.Product.Parameter;
using PGM.Model.ApiPart;

namespace PGM.Interface.ApiPart
{
    public interface IApiPart
    {
        public ResponseApiModel _getFirstApi(RequestApiModel req);
        public ResponseApiModel _getSecondApi(RequestApiModel req);
        public ResponseThirdApiModel _getThirdApi(RequestApiModel req);
        public ResponseApiModel _getFourthApi(reqApiModel req);

    }
}
