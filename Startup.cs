#region Imports
using Autofac;
using PGM.Context;
using PGM.DI;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using PGM.Interface;
using PGM.Repository;
using PGM.DI.NumberThree;
using Autofac.Extensions.DependencyInjection;
using PGM.Config;
using Newtonsoft.Json;
#endregion

namespace PGM
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public ILifetimeScope AutofacContainer { get; set; }

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddControllers();
            services.AddControllers()
                    .AddNewtonsoftJson(options =>
                        options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            );
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "AutofacExamples.PGM", Version = "v1" });
            });

            services.AddDbContext<PGMDbContext>(
                options => options.UseSqlServer(this.Configuration.GetConnectionString("DefaultConnection")));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "AutofacExamples.PGM v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //Load Config Json
            this.AutofacContainer = app.ApplicationServices.GetAutofacRoot();
            var obj = this.AutofacContainer.Resolve<CustomConfig>();
            obj.Load();

        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new ProductModule());
            builder.RegisterModule(new ApiPartModule());
            builder.RegisterModule(new ApiProcessModule());
            builder.RegisterModule(new ShipmentModule());
            builder.RegisterModule(new TripModule());
            builder.RegisterModule(new DataHelperModule());
            builder.RegisterModule(new SenarioModule());
            builder.RegisterModule(new ConditionModule());
            builder.RegisterModule(new NumberThreeModule());
            builder.RegisterModule(new CustomConfigModule());
        }
    }
}
