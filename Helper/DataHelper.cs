﻿

using PGM.Interface;
using PGM.Model;
using PGM.Model.ApiPart;
using PGM.Model.Product.Senario;
using Microsoft.AspNetCore.Session;

namespace PGM.Helper
{
    public class DataHelper : IDataHelper
    {
        public string ReadFile(string path)
        {
            var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), path)
                .Replace('\\', Path.DirectorySeparatorChar);

            var jsonString = File.ReadAllText(folderDetails);

            
            return jsonString;
        }

        public string CompareConditionLogin(List<SenarioListModel> data, SenarioListModel req)
        {
            var result = data.Where(i =>
                    i.ValidateDriverCode == req.ValidateDriverCode &&
                    i.ValidateShip == req.ValidateShip)
                .Select(x => x.process).FirstOrDefault();

            return result;
        }

    }
}
