﻿using PGM.Model;
using PGM.Model.ApiPart;
using PGM.Model.Product.Senario;

namespace PGM.Interface
{
    public interface IDataHelper
    {
        public string ReadFile(string path);

        public string CompareConditionLogin(List<SenarioListModel> data, SenarioListModel req);
    }
}
