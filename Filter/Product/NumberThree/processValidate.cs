﻿
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PGM.Context;
using PGM.Interface.IFilter.IProduct;
using PGM.Model;
using PGM.Model.Product.Parameter;

namespace PGM.Filter.Product
{
    public class processValidate : ISenario
    {
        private PGMDbContext _dbcon { get; }

        public processValidate(PGMDbContext dbcon)
        {
            _dbcon = dbcon;
        }

        public List<CheckInTimeModel> Process(reqNumberModel req)
        {
            List<CheckInTimeModel> result = new List<CheckInTimeModel>();

            var Date = Convert.ToDateTime(req.shipDate);

            return _dbcon.Trip.Include(b => b.Shipment)
                                .ThenInclude(b => b.ShipmentProduct)
                                .Where(x => x.isActive == true
                                            && x.driverCode == req.driverCode
                                            && x.shipDate >= Date)
                                .Select(x => new CheckInTimeModel
                                {
                                    shipDate = x.shipDate,
                                    driverCode = x.driverCode,
                                    tasks = x.Shipment,
                                    checkInTime = x.shipDate,
                                    name = x.Shipment.Select(i => i.ShipmentProduct
                                                                     .Select(p => p.productName).FirstOrDefault()).FirstOrDefault()
                                })
                                .OrderByDescending(x => x.checkInTime)
                                .ToList();

            //throw new NotImplementedException();
        }
    }
}
