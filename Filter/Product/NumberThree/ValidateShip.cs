﻿using PGM.Context;
using PGM.Interface.IFilter.IProduct;
using PGM.Model;
using PGM.Model.Product.Parameter;
using PGM.Model.Product.Senario;

namespace PGM.Filter.Product
{
    public class ValidateShip : INumberThree
    {

        public void Process(ref SenarioListModel senario,reqNumberModel req)
        {
            //วันที่ปัจจุบัน
            DateTime Now = DateTime.Now;

            DateTime date = Convert.ToDateTime(req.shipDate);

            double total = (date - Now).TotalDays;

            //ไม่ใช่เมื่อวาน
            if (total > 0)
                senario.ValidateShip = "y";
            else
                senario.ValidateShip = "n";
        }
    }
}
