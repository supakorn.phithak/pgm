﻿using PGM.Context;
using PGM.Interface.IFilter.IProduct;
using PGM.Model;
using PGM.Model.Product.Parameter;
using PGM.Model.Product.Senario;

namespace PGM.Filter.Product
{
    public class ValidateDriverCode : INumberThree
    {

        public void Process(ref SenarioListModel senario, reqNumberModel req)
        {
            //driver ID ห้าม empty
            if(!string.IsNullOrEmpty(req.driverCode))
                senario.ValidateDriverCode = "y";
            else
                senario.ValidateDriverCode = "n";

        }
    }
}
