﻿using Microsoft.EntityFrameworkCore;
using PGM.Context;
using PGM.Context.EF;
using PGM.Interface.IFilter.IProduct;
using PGM.Model;
using PGM.Model.Product.Parameter;

namespace PGM.Filter.Product
{
    public class FilterShipment : IShipment
    {
        private PGMDbContext _dbcon { get; }

        public FilterShipment(PGMDbContext dbcon)
        {
            _dbcon = dbcon;
        }

        public List<ResponseModel> Process(string shipDate)
        {
            List<ResponseModel> result = new List<ResponseModel>();

            var date = Convert.ToDateTime(shipDate);
            result = _dbcon.Trip
                            .Include(b => b.Shipment.Where(i => i.isActive == true))
                            .ThenInclude(b => b.ShipmentProduct.Where(i => i.statusCode == "ship"))
                            .Where(x => x.shipDate >= date)
                            .Select(i => new ResponseModel { Data = i })
                            .ToList();

            return result;

        }
    }
}
