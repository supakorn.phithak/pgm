﻿
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PGM.Context;
using PGM.Interface.IFilter.IProduct;
using PGM.Model;
using PGM.Model.Product.Parameter;

namespace PGM.Filter.Product
{
    public class FilterTrip : ITrip
    {
        private PGMDbContext _dbcon { get; }

        public FilterTrip(PGMDbContext dbcon)
        {
            _dbcon = dbcon;
        }
        public List<ResponseModel> Process(reqNumberModel req)
        {
            List<ResponseModel> result = new List<ResponseModel>();

            var date = Convert.ToDateTime(req.shipDate);
            result = _dbcon.Trip
                            .Include(b => b.Shipment.Where(i => i.isActive == true))
                            .ThenInclude(b => b.ShipmentProduct.Where(i => i.statusCode == "ship"))
                            .Where(x => x.driverCode == req.driverCode.Trim() && 
                                        x.shipDate >= date)
                            .Select(i => new ResponseModel { Data = i })
                            .ToList();

            return result;
        }
    }
}
