﻿
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PGM.Config;
using PGM.Context;
using PGM.Interface.IFilter.IApiPart;
using PGM.Interface.IFilter.IProduct;
using PGM.Model;
using PGM.Model.ApiPart;
using PGM.Model.Product.Parameter;
using PGM.Service;

namespace PGM.Filter.ApiPart
{
    public class FilterFirstApi : IApi
    {
        private readonly CustomConfig _configData;

        public FilterFirstApi(CustomConfig configData)
        {
            _configData = configData;
        }

        public ResponseApiModel Process(RequestApiModel req)
        {
            ResponseApiModel res = new ResponseApiModel();

            //get Data จาก load start พร้อมเงื่อนไข
            var _data = _configData._MockData.FirstOrDefault(x => x.user == req.user && x.date == req.date);

            if (_data != null) { res.user = req.user; res.status = Status.success.ToString(); res.result_id = _data.id.ToString(); }
            else { res.user = string.Empty; res.status = Status.fail.ToString(); res.result_id = "0"; }

            return res;
            
        }
    }
}
