﻿
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PGM.Config;
using PGM.Context;
using PGM.Interface.IFilter.IApiPart;
using PGM.Interface.IFilter.IProduct;
using PGM.Model;
using PGM.Model.ApiPart;
using PGM.Model.Product.Parameter;
using PGM.Service;

namespace PGM.Filter.ApiPart
{
    public class FilterThirdApi : IApi
    {
        private readonly CustomConfig _configData;

        public FilterThirdApi(CustomConfig configData)
        {
            _configData = configData;
        }

        public ResponseApiModel Process(RequestApiModel req)
        {
            ResponseApiModel res = new ResponseApiModel();

            //mock response
            res.status = Status.success.ToString();
            res.result_id = req.result_id.ToString();

            return res;
            
        }
    }
}
