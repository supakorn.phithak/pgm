﻿
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PGM.Config;
using PGM.Context;
using PGM.Interface;
using PGM.Interface.IFilter.IApiPart;
using PGM.Interface.IFilter.IProduct;
using PGM.Model;
using PGM.Model.ApiPart;
using PGM.Model.Product.Parameter;
using PGM.Service;
using System.Net;
using System.Text;

namespace PGM.Filter.ApiPart
{
    public class FilterFourthApi : IApi
    {
        private readonly CustomConfig _configData;
        private IDataHelper _dataHelper { get; set; }
        public FilterFourthApi(CustomConfig configData, IDataHelper dataHelper)
        {
            _configData = configData;
            _dataHelper = dataHelper;
        }

        public ResponseApiModel Process(RequestApiModel req)
        {
            ResponseApiModel res = new ResponseApiModel();

            //step 1 choice 4/d
            string _reqFirst = _configData._ip + req.user + '/' + req.date;

            var _responseFirst = JsonConvert.DeserializeObject<ResponseApiModel>(sendApiGet(_reqFirst));


            //step 2 choice 4/e

            if (!string.IsNullOrEmpty(_responseFirst.result_id))
            {
                string _reqSecond = _configData._ip + _responseFirst.result_id + "/status";
                

                //loop 30 sec
                for (int i = 1; i <= 30; i++)
                {
                    Task.Delay(1000).Wait();
                    var _responseSecond = JsonConvert.DeserializeObject<ResponseApiModel>(sendApiGet(_reqSecond));

                    //found data break process
                    if (!string.IsNullOrEmpty(_responseSecond.result_id) || _responseSecond.result_id != "0" )
                    {
                        string _reqThird = _configData._ip + _responseSecond.result_id;
                        //step 3 send choice 4/f 
                        res = JsonConvert.DeserializeObject<ResponseApiModel>(sendApiGet(_reqThird));
                        break;
                    }

                    else
                    {
                        res.status = Status.fail.ToString();
                    }
                }
            }
            else
            {
                res.status=Status.fail.ToString();
            }

          
            res.status = Status.success.ToString();
            res.result = res;

            return res;
            
        }
    
        public string sendApiGet(string reqUrl)
        {
            var _req = (HttpWebRequest)WebRequest.Create(reqUrl);

            var _response = (HttpWebResponse)_req.GetResponse();

            var responseString = new StreamReader(_response.GetResponseStream()).ReadToEnd();

            return responseString;
         }
    }
}