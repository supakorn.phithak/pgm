﻿
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PGM.Config;
using PGM.Context;
using PGM.Interface;
using PGM.Interface.IFilter.IApiPart;
using PGM.Interface.IFilter.IProduct;
using PGM.Model;
using PGM.Model.ApiPart;
using PGM.Model.Product.Parameter;
using PGM.Service;

namespace PGM.Filter.ApiPart
{
    public class FilterSecondApi : IApi
    {
        private readonly CustomConfig _configData;
        public FilterSecondApi(CustomConfig configData)
        {
            _configData = configData;
        }

        public ResponseApiModel Process(RequestApiModel req)
        {
            ResponseApiModel res = new ResponseApiModel();

            Thread.Sleep(1000);

            //mock response
            res.user = req.user;
            res.status = Status.success.ToString();
            res.result_id = "1";

            return res;
            
        }
    }
}
