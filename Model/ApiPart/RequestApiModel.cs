﻿using PGM.Context.EF;
using System.ComponentModel;

namespace PGM.Model.ApiPart
{
    public class RequestApiModel
    {
        [Description("ชื่อผู้ใช้งาน")]
        public string user { get; set; }

        [Description ("วันที่")]
        public string date { get; set; }

        [Description ("ID reference สำหรับผลลัพธ์")]
        public int result_id { get; set; }

    }
}
