﻿namespace PGM.Model.ApiPart
{
    public class MockData
    {
        public int id { get; set; }

        public string user { get; set; }

        public string date { get; set; }
    }
}
