﻿using PGM.Context.EF;
using System.ComponentModel;

namespace PGM.Model.ApiPart
{
    public class ResponseApiModel
    {
        [Description("ชื่อผู้ใช้งาน")]
        public string user { get; set; }

        [Description("success หรือ fail")]
        public string status { get; set; }

        [Description ("ID reference สำหรับผลลัพธ")]
        public string result_id { get; set; }

        [Description("ผลลัพธ์ที่ได้")]
        public object result { get; set; }

    }
}
