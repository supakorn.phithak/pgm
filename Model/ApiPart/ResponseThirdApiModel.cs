﻿using PGM.Context.EF;
using System.ComponentModel;

namespace PGM.Model.ApiPart
{
    public class ResponseThirdApiModel
    {
        [Description("User ที่ขอผลลัพธ์")]
        public string user { get; set; }

        [Description ("ID reference สำหรับผลลัพธ")]
        public string result_id { get; set; }

    }
}
