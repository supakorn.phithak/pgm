﻿using PGM.Context.EF;

namespace PGM.Model
{
    public class CheckInTimeModel
    {
        public DateTime shipDate { get; set; }

        public string driverCode { get; set; }

        public object tasks { get; set; }

        public DateTime checkInTime { get; set; }
        
        public string name { get; set; }
    }

}
