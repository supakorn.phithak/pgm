﻿
using Autofac;
using PGM.Filter.ApiPart;
using PGM.Filter.Product;
using PGM.Interface;
using PGM.Interface.IFilter.IApiPart;
using PGM.Repository;

namespace PGM.DI
{
    public class ApiProcessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //Resolve Key by json.config
            builder.RegisterType<FilterFirstApi>().Keyed<IApi>("FilterFirstApi").InstancePerDependency();
            builder.RegisterType<FilterSecondApi>().Keyed<IApi>("FilterSecondApi").InstancePerDependency();
            builder.RegisterType<FilterThirdApi>().Keyed<IApi>("FilterThirdApi").InstancePerDependency();
            builder.RegisterType<FilterFourthApi>().Keyed<IApi>("FilterFourthApi").InstancePerDependency();
        }
    }
}
