﻿
using Autofac;
using PGM.Filter.Product;
using PGM.Interface;
using PGM.Interface.IFilter.IProduct;
using PGM.Repository;

namespace PGM.DI
{
    public class TripModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //Resolve Key by json.config
            builder.RegisterType<FilterTrip>().Keyed<ITrip>("FilterTrip").InstancePerDependency();
        }
    }
}
