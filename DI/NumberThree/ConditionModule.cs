﻿
using Autofac;
using PGM.Filter.Product;
using PGM.Interface.IFilter.IProduct;

namespace PGM.DI.NumberThree
{
    public class ConditionModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<ValidateShip>().Keyed<INumberThree>("ValidateShip").InstancePerDependency();
            builder.RegisterType<ValidateDriverCode>().Keyed<INumberThree>("ValidateDriverCode").InstancePerDependency();
           
        }
    }
}
