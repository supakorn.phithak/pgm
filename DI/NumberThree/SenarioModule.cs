﻿

using Autofac;
using PGM.Filter.Product;
using PGM.Interface.IFilter.IProduct;

namespace PGM.DI.NumberThree
{
    public class SenarioModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<processValidate>().Keyed<ISenario>("processValidate").InstancePerDependency();
            builder.RegisterType<processFail>().Keyed<ISenario>("processFail").InstancePerDependency();

        }
    }
}
