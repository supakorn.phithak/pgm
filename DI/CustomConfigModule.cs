﻿using Autofac;
using PGM.Config;

namespace PGM.DI
{
    public class CustomConfigModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<CustomConfig>().SingleInstance();
        }
    }
}
