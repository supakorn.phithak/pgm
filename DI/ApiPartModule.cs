﻿
using Autofac;
using PGM.Interface.ApiPart;
using PGM.Repository;

namespace PGM.DI
{
    public class ApiPartModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<ApiPartRepository>().As<IApiPart>();

        }
    }
}
