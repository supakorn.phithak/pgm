﻿
using Autofac;
using PGM.Interface;
using PGM.Repository;

namespace PGM.DI
{
    public class ProductModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<ProductRepository>().As<IProduct>();

            // Other Lifetime
            // Transient
            builder.RegisterType<ProductRepository>().As<IProduct>()
                .InstancePerDependency();

            //// Scoped
            builder.RegisterType<ProductRepository>().As<IProduct>()
                .InstancePerLifetimeScope();

            //// Singleton
            builder.RegisterType<ProductRepository>().As<IProduct>()
                .SingleInstance();
        }
    }
}
