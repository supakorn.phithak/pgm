﻿
using Autofac;
using PGM.Filter.Product;
using PGM.Interface;
using PGM.Interface.IFilter.IProduct;
using PGM.Repository;

namespace PGM.DI
{
    public class NumberThreeModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //Resolve Key by json.config
            builder.RegisterType<ValidateDriverCode>().Keyed<INumberThree>("ValidateDriverCode").InstancePerDependency();
            builder.RegisterType<ValidateShip>().Keyed<INumberThree>("ValidateShip").InstancePerDependency();
        }
    }
}
