﻿
using Autofac;
using PGM.Helper;
using PGM.Interface;

namespace PGM.DI
{
    public class DataHelperModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<DataHelper>().As<IDataHelper>();

        }
    }
}
