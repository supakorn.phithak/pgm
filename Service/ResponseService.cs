﻿namespace PGM.Service
{
    public class ResponseService <T>
    {
        public int Status { get; set; }
        public string StatusMessage { get; set; }
        public string Message { get; set; }

        public List<T> Data { get; set; }

        public ResponseService(List<T> data, int status, string message)
        {
            Status = status;
            Message = message;
            Data = data;
        }

        public ResponseService()
        {
        }
    }
}