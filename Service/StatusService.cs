﻿namespace PGM.Service
{
    public enum Status
    {
        process = 0,
        complete = 1,
        error = 2,
        success = 3,
        fail = 4
    }
}
