USE [PGM_Test]
GO
/****** Object:  Table [dbo].[Shipment]    Script Date: 6/15/2022 5:05:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shipment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[tripID] [int] NOT NULL,
	[shipmentNumber] [varchar](255) NULL,
	[pickUpTime] [datetime] NOT NULL,
	[dropOffTime] [datetime] NOT NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK_Shipment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShipmentProduct]    Script Date: 6/15/2022 5:05:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShipmentProduct](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[shipmentID] [int] NULL,
	[barcode] [varchar](255) NULL,
	[productName] [varchar](255) NULL,
	[qty] [int] NULL,
	[statusCode] [nvarchar](max) NULL,
	[isActive] [bit] NOT NULL,
 CONSTRAINT [PK_ShipmentProduct] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Trip]    Script Date: 6/15/2022 5:05:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Trip](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[shipDate] [datetime] NULL,
	[driverCode] [varchar](255) NULL,
	[isActive] [bit] NOT NULL,
 CONSTRAINT [PK_Trip] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Shipment] ON 

INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (1, 1, N'PBR2204-0000018', CAST(N'2022-04-20T06:00:00.000' AS DateTime), CAST(N'2022-04-20T11:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (2, 2, N'PC52204-0000002', CAST(N'2022-04-20T06:00:00.000' AS DateTime), CAST(N'2022-04-20T11:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (5, 3, N'PAR2204-0000001', CAST(N'2022-04-21T06:00:00.000' AS DateTime), CAST(N'2022-04-21T16:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (6, 4, N'PDC2204-0000009', CAST(N'2022-04-21T06:00:00.000' AS DateTime), CAST(N'2022-04-21T13:00:00.000' AS DateTime), 0)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (7, 5, N'PBR2109-0000156', CAST(N'2022-04-21T13:00:00.000' AS DateTime), CAST(N'2022-04-21T16:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (8, 6, N'PC22204-0000005', CAST(N'2022-04-26T10:00:00.000' AS DateTime), CAST(N'2022-04-26T11:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (9, 7, N'PC22204-0000004', CAST(N'2022-04-26T10:00:00.000' AS DateTime), CAST(N'2022-04-26T11:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (10, 8, N'PBR2204-0000028', CAST(N'2022-04-26T13:00:00.000' AS DateTime), CAST(N'2022-04-26T16:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (11, 9, N'PC22204-0000002', CAST(N'2022-04-26T06:00:00.000' AS DateTime), CAST(N'2022-04-26T11:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (12, 10, N'PAR2204-0000003', CAST(N'2022-04-26T06:00:00.000' AS DateTime), CAST(N'2022-04-26T09:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (13, 11, N'PBR2204-0000027', CAST(N'2022-04-26T16:00:00.000' AS DateTime), CAST(N'2022-04-26T20:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (14, 12, N'PBR2204-0000029', CAST(N'2022-04-26T16:00:00.000' AS DateTime), CAST(N'2022-04-26T18:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (15, 13, N'PBR2204-0000030', CAST(N'2022-04-26T16:00:00.000' AS DateTime), CAST(N'2022-04-26T13:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (16, 14, N'PC22204-0000008', CAST(N'2022-04-26T12:00:00.000' AS DateTime), CAST(N'2022-04-26T11:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (17, 15, N'PDC2204-0000013', CAST(N'2022-04-26T14:00:00.000' AS DateTime), CAST(N'2022-04-26T11:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (18, 16, N'PC22204-0000003', CAST(N'2022-04-26T06:00:00.000' AS DateTime), CAST(N'2022-04-26T11:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (19, 17, N'PC22204-0000006', CAST(N'2022-04-26T10:00:00.000' AS DateTime), CAST(N'2022-04-26T11:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (20, 18, N'PC22204-0000007', CAST(N'2022-04-26T08:00:00.000' AS DateTime), CAST(N'2022-04-26T11:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (21, 19, N'PBR2204-0000025', CAST(N'2022-04-26T06:00:00.000' AS DateTime), CAST(N'2022-04-26T13:00:00.000' AS DateTime), 0)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (22, 20, N'PDC2204-0000014', CAST(N'2022-04-26T06:00:00.000' AS DateTime), CAST(N'2022-04-26T09:00:00.000' AS DateTime), 0)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (23, 21, N'PBR2204-0000034', CAST(N'2022-04-28T13:00:00.000' AS DateTime), CAST(N'2022-04-28T16:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (24, 22, N'PDC2204-0000019', CAST(N'2022-04-29T13:00:00.000' AS DateTime), CAST(N'2022-04-29T18:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (25, 23, N'PBR2204-0000048', CAST(N'2022-04-30T08:00:00.000' AS DateTime), CAST(N'2022-04-30T16:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (26, 24, N'PBR2204-0000040', CAST(N'2022-04-30T06:00:00.000' AS DateTime), CAST(N'2022-04-30T09:00:00.000' AS DateTime), 0)
INSERT [dbo].[Shipment] ([ID], [tripID], [shipmentNumber], [pickUpTime], [dropOffTime], [isActive]) VALUES (27, 25, N'PBR2204-0000044', CAST(N'2022-04-30T06:00:00.000' AS DateTime), CAST(N'2022-04-30T16:00:00.000' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Shipment] OFF
GO
SET IDENTITY_INSERT [dbo].[ShipmentProduct] ON 

INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (1, 1, NULL, N'Delivery Center', 1, N'ship', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (2, 2, NULL, N'?????? ??????????? ?????', 2, N'ship', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (3, 3, NULL, N'Delivery Center', 3, N'ship', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (4, 4, NULL, N'?????? ??????????? ?????', 2, N'ship', 0)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (5, 5, NULL, N'Delivery Center', 3, N'ship', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (6, 6, NULL, N'Delivery Center', 1, N'ship', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (7, 7, NULL, N'Delivery Center', 4, N'ship', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (8, 8, NULL, N'Delivery Center', 2, N'road', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (9, 9, NULL, N'Delivery Center', 1, N'road', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (10, 10, NULL, N'Delivery Center', 1, N'road', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (11, 11, NULL, N'Delivery Center', 2, N'road', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (12, 12, NULL, N'Delivery Center', 2, N'road', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (13, 13, NULL, N'Delivery Center', 2, N'road', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (14, 14, NULL, N'Delivery Center', 3, N'road', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (15, 15, NULL, N'Delivery Center', 2, N'fly', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (16, 16, NULL, N'Delivery Center', 1, N'fly', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (17, 17, NULL, N'Delivery Center', 2, N'fly', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (18, 18, NULL, N'Delivery Center', 2, N'fly', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (19, 19, NULL, N'Delivery Center', 4, N'fly', 0)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (20, 20, NULL, N'?????? ??????????? ?????', 3, N'ship', 0)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (21, 21, NULL, N'Delivery Center', 4, N'road', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (22, 22, NULL, N'Delivery Center', 1, N'road', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (23, 23, NULL, N'Delivery Center', 2, N'ship', 1)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (24, 24, NULL, N'Delivery Center', 1, N'ship', 0)
INSERT [dbo].[ShipmentProduct] ([ID], [shipmentID], [barcode], [productName], [qty], [statusCode], [isActive]) VALUES (25, 25, NULL, N'Delivery Center', 1, N'fly', 1)
SET IDENTITY_INSERT [dbo].[ShipmentProduct] OFF
GO
SET IDENTITY_INSERT [dbo].[Trip] ON 

INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (1, CAST(N'2022-04-20T11:00:00.000' AS DateTime), N'D009504', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (2, CAST(N'2022-04-20T11:00:00.000' AS DateTime), N'D009457', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (3, CAST(N'2022-04-21T16:00:00.000' AS DateTime), N'D009504', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (4, CAST(N'2022-04-21T13:00:00.000' AS DateTime), N'D006105', 0)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (5, CAST(N'2022-04-21T16:00:00.000' AS DateTime), N'D009513', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (6, CAST(N'2022-04-26T11:00:00.000' AS DateTime), N'D220003', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (7, CAST(N'2022-04-26T11:00:00.000' AS DateTime), N'D220003', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (8, CAST(N'2022-04-26T16:00:00.000' AS DateTime), N'D009494', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (9, CAST(N'2022-04-26T11:00:00.000' AS DateTime), N'D220003', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (10, CAST(N'2022-04-26T09:00:00.000' AS DateTime), N'D009500', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (11, CAST(N'2022-04-26T20:00:00.000' AS DateTime), N'D009513', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (12, CAST(N'2022-04-26T18:00:00.000' AS DateTime), N'D009513', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (13, CAST(N'2022-04-26T13:00:00.000' AS DateTime), N'D210075', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (14, CAST(N'2022-04-26T11:00:00.000' AS DateTime), N'D220003', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (15, CAST(N'2022-04-26T11:00:00.000' AS DateTime), N'D220005', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (16, CAST(N'2022-04-26T11:00:00.000' AS DateTime), N'D220003', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (17, CAST(N'2022-04-26T11:00:00.000' AS DateTime), N'D220003', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (18, CAST(N'2022-04-26T11:00:00.000' AS DateTime), N'D220003', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (19, CAST(N'2022-04-26T13:00:00.000' AS DateTime), N'D009504', 0)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (20, CAST(N'2022-04-26T09:00:00.000' AS DateTime), N'D006105', 0)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (21, CAST(N'2022-06-28T16:00:00.000' AS DateTime), N'D220004', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (22, CAST(N'2022-04-29T18:00:00.000' AS DateTime), N'D220005', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (23, CAST(N'2022-04-30T16:00:00.000' AS DateTime), N'D220004', 1)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (24, CAST(N'2022-04-30T09:00:00.000' AS DateTime), N'D009500', 0)
INSERT [dbo].[Trip] ([ID], [shipDate], [driverCode], [isActive]) VALUES (25, CAST(N'2022-06-30T16:00:00.000' AS DateTime), N'D220004', 1)
SET IDENTITY_INSERT [dbo].[Trip] OFF
GO
ALTER TABLE [dbo].[Shipment] ADD  CONSTRAINT [DF_Shipment_pickUpTime]  DEFAULT (getdate()) FOR [pickUpTime]
GO
ALTER TABLE [dbo].[Shipment] ADD  CONSTRAINT [DF_Shipment_dropOffTime]  DEFAULT (getdate()) FOR [dropOffTime]
GO
ALTER TABLE [dbo].[Shipment] ADD  CONSTRAINT [DF_Shipment_isActive]  DEFAULT ((1)) FOR [isActive]
GO
ALTER TABLE [dbo].[ShipmentProduct] ADD  CONSTRAINT [DF_ShipmentProduct_isActive]  DEFAULT ((1)) FOR [isActive]
GO
ALTER TABLE [dbo].[Trip] ADD  CONSTRAINT [DF_Trip_isActive]  DEFAULT ((1)) FOR [isActive]
GO
