﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PGM.Config;
using PGM.Context;
using PGM.Context.EF;
using PGM.Interface;
using PGM.Interface.ApiPart;
using PGM.Interface.IFilter.IApiPart;
using PGM.Interface.IFilter.IProduct;
using PGM.Model;
using PGM.Model.ApiPart;
using PGM.Model.Product.Parameter;
using PGM.Model.Product.Senario;

namespace PGM.Repository
{
    public class ApiPartRepository : IApiPart
    {
        private IDataHelper _dataHelper { get; set; }

        private ILifetimeScope _autofac { get; set; }

        //กรณีที่ไม่ได้อ่าน config จาก json เราจะกำหนดเงื่อนไขผ่าน interface 
        //โดยไม่ต้อง resolve จาก key
        private readonly IApi[] _filter;

        private readonly CustomConfig _configData;

        public ApiPartRepository(
            IApi[] filter,
            ILifetimeScope autofac,
            IDataHelper dataHelper,
            CustomConfig configData)
        {
            _autofac = autofac;
            _filter = filter;
            _dataHelper = dataHelper;
            _configData = configData;
        }

        public ResponseApiModel _getFirstApi(RequestApiModel req)
        {
            ResponseApiModel result = new ResponseApiModel();

            //เลือก filter 
            //api เส้นแรก เลือกใช้ filter เฉพาะตัวแรก
            var filter = _configData._apiPart.FirstOrDefault(x => x.SeqNo == 1);

            var build = _autofac.BeginLifetimeScope();

            var obj = build.ResolveKeyed<IApi>(filter.process);

            result = obj.Process(req);


            return result;
        }

        public ResponseApiModel _getSecondApi(RequestApiModel req)
        {
            ResponseApiModel result = new ResponseApiModel();

            //เฉพาะเงื่อนไขที่สอง
            var filter = _configData._apiPart.FirstOrDefault(x => x.SeqNo == 2);

            var build = _autofac.BeginLifetimeScope();

            var obj = build.ResolveKeyed<IApi>(filter.process);

            result = obj.Process(req);


            return result;
        }

        public ResponseThirdApiModel _getThirdApi(RequestApiModel req)
        {
            ResponseThirdApiModel result = new ResponseThirdApiModel();

            ResponseApiModel _data = new ResponseApiModel();

            //เฉพาะเงื่อนไขที่สาม
            var filter = _configData._apiPart.FirstOrDefault(x => x.SeqNo == 3);

            var build = _autofac.BeginLifetimeScope();

            var obj = build.ResolveKeyed<IApi>(filter.process);

            _data = obj.Process(req);

            //mock dummy
            result.result_id = _data.result_id;
            result.user = "dummy";

            return result;
        }

        public ResponseApiModel _getFourthApi(reqApiModel req)
        {
            ResponseApiModel result = new ResponseApiModel();

            RequestApiModel request = new RequestApiModel();

            request.user = req.username;
            request.date = req.date;

            //เฉพาะเงื่อนไขที่สอง
            var filter = _configData._apiPart.FirstOrDefault(x => x.SeqNo == 4);

            var build = _autofac.BeginLifetimeScope();

            var obj = build.ResolveKeyed<IApi>(filter.process);

            result = obj.Process(request);


            return result;
        }
    }
}
