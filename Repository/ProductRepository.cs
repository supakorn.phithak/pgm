﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PGM.Config;
using PGM.Context;
using PGM.Context.EF;
using PGM.Interface;
using PGM.Interface.IFilter.IProduct;
using PGM.Model;
using PGM.Model.Product.Parameter;
using PGM.Model.Product.Senario;

namespace PGM.Repository
{
    public class ProductRepository : IProduct
    {
        private ILifetimeScope _autofac { get; set; }

        private IDataHelper _dataHelper { get; set; }

        private readonly CustomConfig _configData;

        public ProductRepository(
            ILifetimeScope autofac,
            IDataHelper dataHelper,
            CustomConfig configData)
        {
            _autofac = autofac;
            _dataHelper = dataHelper;
            _configData = configData;
        }

        public List<ResponseModel> _getListShipment(string shipDate)
        {
            List<ResponseModel> shipment = new List<ResponseModel>();

            var build = _autofac.BeginLifetimeScope();

            var SeqList = _configData._shipment;

            foreach (var item in SeqList)
            {
                var obj = build.ResolveKeyed<IShipment>(item.process);
                shipment = obj.Process(shipDate);
            }

            return shipment;
        }

        public List<ResponseModel> _getListTrip(reqNumberModel req)
        {
            List<ResponseModel> Trip = new List<ResponseModel>();

            var build = _autofac.BeginLifetimeScope();

            var SeqList = _configData._trip;

            foreach (var item in SeqList)
            {
                var obj = build.ResolveKeyed<ITrip>(item.process);
                Trip = obj.Process(req);
            }

            return Trip;
        }

        public List<CheckInTimeModel> _getNumberThree(reqNumberModel req)
        {
            List<CheckInTimeModel> result = new List<CheckInTimeModel>();

            SenarioListModel senario = new SenarioListModel();

            var build = _autofac.BeginLifetimeScope();

            var SeqList = _configData._numberThree;
            
            foreach (var item in SeqList)
            {
                var obj = build.ResolveKeyed<INumberThree>(item.process);
                obj.Process(ref senario,req);
            }

            if(senario != null)
            {
                //get senario
                var SenarioList = JsonConvert.DeserializeObject<List<SenarioListModel>>
                    (_dataHelper.ReadFile($"Config\\Product\\NumberThree\\Senario.json"));

                //compare Senario
                var _compareSenario = _dataHelper.CompareConditionLogin(SenarioList, senario);

                //ResolveKeyed Class
                var obj = build.ResolveKeyed<ISenario>(_compareSenario);

                //Get Result From Class
                result = obj.Process(req);
            }


            return result;
        }

    }
}
