﻿
using Newtonsoft.Json;
using PGM.Interface;
using PGM.Model;
using PGM.Model.ApiPart;
using PGM.Model.Product.Validate;
using System.Linq;

namespace PGM.Config
{
    public class CustomConfig
    {
        private readonly IDataHelper _dataHelper;

        public CustomConfig(IDataHelper dataHelper)
        {
            _dataHelper = dataHelper;
        }

        //load data config 
        //ex.Data api
        public void Load()
        {
            Thread t1 = new Thread(new ThreadStart(NumberThree));
            Thread t2 = new Thread(new ThreadStart(Shipment));
            Thread t3 = new Thread(new ThreadStart(Trip));
            Thread t4 = new Thread(new ThreadStart(ApiPart)); //mock data
            Thread t5 = new Thread(new ThreadStart(ApiPartConfig));

            t1.Start();
            t2.Start();
            t3.Start();
            t4.Start();
            t5.Start();

            this._ip = "https://localhost:7104/resultdata/";
        }

        public IOrderedEnumerable<FilterModel> _trip { get; set; }
        public IOrderedEnumerable<FilterModel> _shipment { get; set; }
        public IOrderedEnumerable<FilterModel> _numberThree { get; set; }
        public IOrderedEnumerable<FilterModel> _apiPart { get; set; }
        public string _ip { get; set; }
        public List<MockData> _MockData { get; set; }

        public void Trip()
        {
            var list = JsonConvert.DeserializeObject<List<FilterModel>>(
                 _dataHelper.ReadFile($"Config\\Product\\Trip\\getFilter.json")
                 ).OrderBy(x => x.SeqNo);

            this._trip = list;
        }
        public void Shipment()
        {
            var list = JsonConvert.DeserializeObject<List<FilterModel>>(
                 _dataHelper.ReadFile($"Config\\Product\\Shipment\\getFilter.json")
                 ).OrderBy(x => x.SeqNo);

            this._shipment = list;
        }
        public void NumberThree()
        {
            var list = JsonConvert.DeserializeObject<List<FilterModel>>(
                 _dataHelper.ReadFile($"Config\\Product\\NumberThree\\getFilter.json")
                 ).OrderBy(x => x.SeqNo);

            this._numberThree = list;
        }
        public void ApiPartConfig()
        {
            var list = JsonConvert.DeserializeObject<List<FilterModel>>(
                _dataHelper.ReadFile($"Config\\ApiPart\\getFilter.json")
                ).OrderBy(x => x.SeqNo);

            this._apiPart = list;
        }
        public void ApiPart()
        {
            List<MockData> md = new List<MockData>()
            {
                new MockData { id=1 , user = "test01" , date = "20220101"},
                new MockData { id=2 , user = "test02" , date = "20220102"},
                new MockData { id=3 , user = "test03" , date = "20220103"},
                new MockData { id=4 , user = "test04" , date = "20220104"},
                new MockData { id=5 , user = "test05" , date = "20220105"},
            };

            this._MockData = md;
        }
    }
}
