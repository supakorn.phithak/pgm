﻿using PGM.Model;
using Microsoft.EntityFrameworkCore;
using PGM.Context.EF;

namespace PGM.Context
{
    public class PGMDbContext : DbContext
    {
        public PGMDbContext(DbContextOptions<PGMDbContext> options ) 
            : base(options)
        {

        }

        public virtual DbSet<TripModel> Trip { get; set; }

        public virtual DbSet<ShipmentModel> Shipment { get; set;  }

        public virtual DbSet<ShipmentProductModel> ShipmentProduct { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<TripModel>(entity =>
            //{
            //    entity.ToTable("Trip");

            //    entity.HasKey(e => new { e.ID });

            //    entity.HasOne(d => d.Shipment)
            //       .WithMany(g => g.Trip)
            //       .HasForeignKey(s => s.ID);

            //});

            //modelBuilder.Entity<ShipmentModel>(entity =>
            //{
            //    entity.ToTable("Shipment");

            //    entity.HasKey(e => new { e.ID });

            //    entity.HasOne(d => d.ShipmentProduct)
            //       .WithMany(g => g.Shipment)
            //       .HasForeignKey(s => s.ID);
            //});

        }

    }
}