﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PGM.Context.EF
{
    public class ShipmentModel
    {

        public int ID { get; set; }

        public int tripID { get; set; }

        public string shipmentNumber { get; set; }

        public DateTime pickUpTime { get; set; }

        public DateTime dropOffTime { get; set; }

        public bool isActive { get; set; }

        [ForeignKey("ID")]
        public TripModel Trip { get; set; }

        public List<ShipmentProductModel> ShipmentProduct { get; set; }
    }
}
