﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PGM.Context.EF
{
    public class ShipmentProductModel
    {

        public int ID { get; set; }

        public int shipmentID { get; set; }

        public string barcode { get; set; }

        public string productName { get; set; }

        public int qty { get; set; }

        public string statusCode { get; set; }

        public bool isActive { get; set; }

        [ForeignKey("ID")]
        public ShipmentModel Shipment { get; set; }
    }
}
