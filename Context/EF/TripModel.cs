﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PGM.Context.EF
{
    public class TripModel
    {

        public int ID { get; set; }

        public DateTime shipDate { get; set; }

        public string driverCode { get; set; }

        public bool isActive { get; set; }

        public List<ShipmentModel> Shipment { get; set; }

    }
}
