using Microsoft.AspNetCore.Mvc;
using PGM.Context.EF;
using PGM.Interface;
using PGM.Interface.ApiPart;
using PGM.Model;
using PGM.Model.ApiPart;
using PGM.Model.Product.Parameter;
using PGM.Service;

namespace PGM.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ApiPartController : ControllerBase
    {

        private readonly ILogger<ApiPartController> _logger;

        private readonly IApiPart _apiPart;
        public ApiPartController(ILogger<ApiPartController> logger, IApiPart apiPart)
        {
            _logger = logger;
            _apiPart = apiPart;
        }

        [HttpGet("/resultdata/{user}/{yyyyMMdd}", Name = "getFirstApi")]
        public ResponseApiModel _FirstApi(string user,string yyyyMMdd)
        {
            ResponseApiModel response = new ResponseApiModel();

            RequestApiModel req = new RequestApiModel();
            try
            {
                if (!string.IsNullOrEmpty(user) && !string.IsNullOrEmpty(yyyyMMdd))
                {
                    req.user = user;
                    req.date = yyyyMMdd;

                    response = _apiPart._getFirstApi(req);

                    if (response == null)
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                response.status = "fail";
                response.result_id = "0";
            }

            return response;
        }


        [HttpGet("/resultdata/{result_id}/status", Name = "getSecondApi")]
        public ResponseApiModel _SecondApi(int result_id)
        {
            ResponseApiModel response = new ResponseApiModel();

            RequestApiModel req = new RequestApiModel();
            try
            {
                if (result_id >= 0)
                {
                    req.result_id = result_id;

                    response = _apiPart._getSecondApi(req);

                    if (response == null)
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                response.status = Status.fail.ToString();
                response.result_id = "0";
            }

            return response;
        }


        [HttpGet("/resultdata/{result_id}", Name = "getThirdApi")]
        public ResponseThirdApiModel _ThirdApi(int result_id)
        {
            ResponseThirdApiModel response = new ResponseThirdApiModel();

            RequestApiModel req = new RequestApiModel();
            try
            {
                if (result_id >= 0)
                {
                    req.result_id = result_id;

                    response = _apiPart._getThirdApi(req);

                    if (response == null)
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                response.user = ex.Message;
                response.result_id = "0";
            }

            return response;
        }


        [HttpPost("/resultdata/LastMethod", Name = "LastMethod")]
        public ResponseApiModel _LastMethod(reqApiModel req)
        {
            ResponseApiModel response = new ResponseApiModel();
            try
            {
                //parameter is empty
                if (string.IsNullOrEmpty(req.username) || string.IsNullOrEmpty(req.date))
                    throw new Exception();

                response = _apiPart._getFourthApi(req);

                if (response == null)
                {
                    throw new Exception();
                }

            }
            catch (Exception ex)
            {
                response.status = Status.fail.ToString() + " " + ex.Message;
            }


            return response;
        }


    }
}