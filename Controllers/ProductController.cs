using Microsoft.AspNetCore.Mvc;
using PGM.Context.EF;
using PGM.Interface;
using PGM.Model;
using PGM.Model.ApiPart;
using PGM.Model.Product.Parameter;
using PGM.Service;

namespace PGM.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {

        private readonly ILogger<ProductController> _logger;

        private readonly IProduct _product;
        public ProductController(ILogger<ProductController> logger, IProduct product)
        {
            _logger = logger;
            _product = product;
        }

        [HttpPost("ListShipment")]
        public ResponseService<ResponseModel> _ListShipment(string shipDate)
        {
            ResponseService<ResponseModel> response = new ResponseService<ResponseModel>();
            List<ResponseModel> products = new List<ResponseModel>();

            try
            {
                if (!string.IsNullOrEmpty(shipDate))
                    products = _product._getListShipment(shipDate);
                else
                    throw new Exception();

                response.Message = "ListShipment";
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
           

            response.Status = products.Count() > 0 ? (int)httpCode.OK : (int)httpCode.Fail;
            response.Data = products;

            return response;
        }

        [HttpPost("ListTrip")]
        public ResponseService<ResponseModel> _ListTrip(reqNumberModel req)
        {
            ResponseService<ResponseModel> response = new ResponseService<ResponseModel>();
            List<ResponseModel> products = new List<ResponseModel>();

            try
            {
                if (!string.IsNullOrEmpty(req.shipDate) && !string.IsNullOrEmpty(req.driverCode))
                    products = _product._getListTrip(req);
                else
                    throw new Exception();

                response.Message = "ListTrip";
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }

            response.Status = products.Count() > 0 ? (int)httpCode.OK : (int)httpCode.Fail;
            response.Data = products;

            return response;
        }

        [HttpPost("ListNumberThree")]
        public ResponseService<CheckInTimeModel> _ListNumberThree(reqNumberModel req)
        {
            ResponseService<CheckInTimeModel> response = new ResponseService<CheckInTimeModel>();
            List<CheckInTimeModel> products = new List<CheckInTimeModel>();

            try
            {
                //parameter is empty
                if (string.IsNullOrEmpty(req.shipDate) || string.IsNullOrEmpty(req.driverCode))
                    throw new Exception();

                products = _product._getNumberThree(req);
                response.Message = "ListNumberThree";

            }
            catch(Exception ex)
            {
                response.Message  = ex.Message;
            }

            response.Status = products.Count() > 0 ? (int)httpCode.OK : (int)httpCode.Fail;
            response.Data = products;

            return response;
        }

    }
}